/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },    
    data: {},
    lon:0,
    lat:0,
    getData: function(){
        navigator.accelerometer.getCurrentAcceleration(function(acceleration){
            app.data.x=acceleration.x;
            $('#ax').html("Acceleration X: "+ app.data.x);
            app.data.y=acceleration.y;
            $('#ay').html("Acceleration Y: "+app.data.y )
            app.data.z=acceleration.z;
            $('#az').html("Acceleration Z: "+app.data.z);
            app.data.timestamp=acceleration.timestamp;
             $('#timestamp').html("Timestamp: "+app.data.timestamp);
        }, function(){
            console.log("Accelerometr error");
        });
        navigator.geolocation.getCurrentPosition(function(position) {
            app.data.latitude=position.coords.latitude
            app.data.longitude=position.coords.longitude
            $('#gps').html("GPS: "+ app.data.latitude + ', ' + app.data.longitude);
            $('#speed').html("Speed: "+ 111.2*Math.sqrt(Math.abs((app.lon - app.data.longitude)*(app.lon - app.data.longitude) + (app.lat - app.data.latitude)*Math.cos(Math.PI*app.lon/180)*(app.lat - app.data.latitude)*Math.cos(Math.PI*app.lon/180)))*60*60+" км/ч");
            app.lon=app.data.longitude
            app.lat=app.data.latitude
           }
        );
        post_data = JSON.stringify(app.data);
        console.log(post_data);
        $.ajax({
            type: "POST",
            url: "https://massgames.ru/data",
            data: post_data, 
            dataType: "json",
            contentType: "application/json",
            success: function(result){
                        console.log("Result: "+result)
                    },
        });
    },
    
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {        
        app.receivedEvent();
    },
    // Update DOM on a Received Event
    receivedEvent: function() {
        setInterval(app.getData, 1000);
    }
};
